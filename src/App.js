
/* eslint-disable */
import './core/styles/App.css';
import { Store , Uuid } from './core/lib/index.js';
import Core from  './core/Core.js'; 

function App() {
  return (
    <div className="App">
        <Core />
    </div>
  );
}

export default App;
