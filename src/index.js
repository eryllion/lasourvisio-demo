/* eslint-disable */
import React from 'react';
import ReactDOM from 'react-dom';
import './core/styles/index.css';
import './core/styles/common.css'; 
import App from './App';


ReactDOM.render(
    <App />
,
  document.getElementById('root')
);

