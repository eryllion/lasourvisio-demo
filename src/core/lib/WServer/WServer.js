/* eslint-disable */
import React from 'react';
import { Store } from '../Tools/Store.js';
import { Events } from '../Events/Events.js'; 
import { CONFIG } from '../../config/config.js'; 
import io from 'socket.io-client';

class WServerClass {
    constructor() {
        this.Reset();
    }
    async Reset() {
        this.socket = null;
        this.socketid = null;
    }

    async Connect( opt = { }) {
        const { query = '', uuid  } = opt; 
        const setting = {
            ...CONFIG.socket_settings,
            query, 
        };
        const  { host, port } = CONFIG.socket_api_connect;
        var url = host + ':' + port 
       
        this.socket = io( url    , setting);
		this.socket.on('connect_error', (m) => {
			//console.log("e", m);
            Events.fire('SOCKET_ERROR', true ); 
		})
        this.socket.on('connected', ({ siteDatas }) => {
            //console.log( 'connected width webserver');
            if ( this.socketid !== '' && this.socketid !== this.socket.id ) Events.fire('SOCKET_ID_CHANGED', this.socketid );
            this.socketid = this.socket.id;
            Store.siteDatas = siteDatas; 
            this.connected = true; 
            Events.fire('SOCKET_CONNECTED', true );
            setTimeout( () => {  this.KeepAlive() ;   }, this.keepAliveDelay );
        

           }); 
           this.socket.on('APP-RECIEVE' , ({ e , datas }) =>   { 
                Events.fire( e, datas );
           });
           this.socket.on( 'ROOM-ON-MESSAGE', (data) => {
               Events.fire('ROOM-ON-MESSAGE', data.message);
           } );
           this.socket.on('user-leaved', (data) => {
               Events.fire('user-leaved' ,  data ); 
           })
    }

    KeepAlive () {
        if (!this.connected) return false;
		this.socket.emit('set/keepconnect/');
		setTimeout(() => {
			this.KeepAlive();
		}, this.keepAliveDelay  ); 
    }


    Reset() {
        this.uid = 0 ; this.socketid = '';
        this.keepAliveDelay = 30000; //30s
        this.connected  = false;
        this.OnConnected = () =>  { } ;
        this.OnDisconnected = () =>  {  };
        //this.ResetEvents();
    }
    Emit ( key, datas ) {
        this.socket.emit( key , datas ); 
    }
}

const WServer = new WServerClass();
export { WServer };