/* eslint-disable */
import React from 'react';
import { WebRtcClass  } from './WebRtcClass.js';
import { Events } from '../Events/Events.js';

export const USERMODEL = { 
    pseudo: '',
    id: '',
    uuid: '',
}

export class VideoRoomClass {
    constructor( roomId = 2000 ) {
        this.roomId = roomId;
        this.WebRTC = null;
        this.indexes = [];
        this.feeds = {};
        this.user_data = {};
        this.OnListChanged = () => {};
        this.OnDelParticipant = () => { }; 

    }
    async Init() {
        this.WebRTC = await new WebRtcClass( this.roomId );
        this.WebRTC.OnAttached = () => { this.WebRTC.Join(); };
        this.WebRTC.OnJoined = () => {
            // au cas où

        }
        this.WebRTC.onLocalStream = ( stream ) => {
            var me = { ...this.user_data, id: this.WebRTC.myid, stream };
            console.log('local stream'); 
            this.SetParticipant( me, 'local' );
           
        }
        this.WebRTC.onRemoteStream = ( stream, user, id ) => {
            var other = { ...user, id, stream };
            this.SetParticipant( other, 'remote' );
        }
        this.WebRTC.OnRemoteLeave = ( { user } ) =>  {
            this.DelParticipant( user );
        }
        this.WebRTC.user_data = this.user_data ; 
        this.WebRTC.Init();

        Events.on('user-leaved', 'VideRoom' , ( data ) => {
            this.DelParticipant( data.user ); 
        })

    }

    async DelParticipant ( user ) {
        const { uuid } = user; 
        this.feeds[ uuid ] = null;
        delete this.feeds[ uuid ];
        var tmp = [];
      
        this.OnDelParticipant( uuid, Object.values( this.feeds  ) ); 
    }

    async SetParticipant ( user, type ) {
        user.type = type;
        const { uuid } = user;
       
        this.feeds[ uuid ] = user;
        this.FireChange();
        
    }
    async FireChange() {
        var list = Object.values( this.feeds );
       

        this.OnListChanged( list );
    }
}
