/* eslint-disable */
import React from 'react';
import { USERMODEL } from './VideoRoom.js';
//import Janus from '../janus/janus.js'; 




var server = null;
server = 'https://lasour.sytes.net/j/8089/janus' ; 	
server = "https://lasour.sytes.net/broadcast/"
var DEBUG = 'none'; 
var MAX_USERS = 4; 

export class WebRtcClass {
    constructor( roomId ) {
        this.janus = null;
        this.opaqueId = 'webrtc-' + Janus.randomString(12) ;
        this.myroom = roomId ;
        this.sfutest = null;
        this.OnAttached = () => { }; 
        this.OnRemoteLeave = () => { }; 
        this.onLocalStream = () => { };
        this.OnJoined = () =>  { }; 
        this.onRemoteStream = () => { }; 
        this.localVideoElement = null; 
        this.myid = null; 
        this.mypvtid = null;
        this.user_data = { pseudo : '', };
        this.feeds = []; 

    }
    async Init() {
        let { janus } = this; 
        if( this.roomId === 0 ) return false;
        if( !Janus.isWebrtcSupported() ) {
            alert("Votre navigateur n'est pas compatible"); 
            return false;
        }
        Janus.init({debug: DEBUG, callback: () => {
                this.janus = new Janus({
                    server,
                    iceServers: [
                        {  urls :'turn:lasour.sytes.net:3478', username: 'azura', credential: 'unimo298'  },
                    ], 
                    success: () => {
                        this.Attach()
                    },
                    error: function(error) {
						//console.log(error);
					},
					destroyed: function() {
						
					}
                });
                    
            }
        });
    }
    


    async Stop( cb = () => { }) {
        await this.sfutest.hangup();
        await this.janus.destroy();
        cb();
    }

    async Attach() {
        let { opaqueId, sfutest  } = this; 
        let { janus } = this; 
        var _this = this; 
        this.janus.attach({
            plugin: "janus.plugin.videoroom",
            opaqueId, 
            success: ( pluginHandle ) => {
                this.sfutest = pluginHandle;
                this.OnAttached();
                //console.log('Attached');

            },
            error: (error) => {  },
            consentDialog: (on) => { },
            mediaState: (medium, on) => { },
            webrtcState: (on) => { },
            onmessage: (msg, jsep) => {
                _this.OnMessage( msg, jsep ); 
            },
            onlocalstream: (stream) => {
                this.localStream = stream;
                this.onLocalStream( stream );
               // Janus.attachMediaStream( this.localVideoElement , stream );
            },
            onremotestream: () => { },
            oncleanup: () => { 
                this.localStream = null; 
            }
        })
    }

    OnMessage( msg, jsep ) {
        const { feeds } = this; 
        const { videoroom = null, id = 0, publishers = null , leaving = null, unpublished = null } = msg;
        //console.log('msg', msg ); 
        if(  videoroom === 'joined') {
                //console.log('joined')
                this.myid = id; 
                this.OnJoined(  ); 
                 this.mypvtid = msg['private_id'];
                this.Publish( true );
                if( publishers !== null ) {
                    var list = publishers;
                    for( let i in list ) {
                        this.NewRemoteFeed( list[i] ); 
                    } 
                }
        } else if ( videoroom === 'destroyed') {

        } else if ( videoroom === 'event') {
            if( publishers !== null ) {
                for( let i in publishers ) {
                    this.NewRemoteFeed( publishers[i] ); 
                }
            } else if (  leaving !== null ) {
                var remoteFeed = null;
                for( let  i in feeds ) {
                    if( feeds[ i ] !== null && feeds[ i ] !== "undefined"    ) {
                        if( typeof feeds[i].rfid !== 'undefined' ) {
                            if( feeds[i].rfid == leaving ) {
                                remoteFeed = feeds[ i ];
                                break;
                            }
                        }
                        
                        
                    }
                }
               
                if( remoteFeed !== null ) {
                    this.OnRemoteLeave(remoteFeed);
                    feeds[remoteFeed.rfindex] = null;
                    remoteFeed.detach();
                }
            } else if ( unpublished !== null ) {
                if( unpublished === 'ok') {
                    this.sfutest.hangup();
                    return;
                }
                var remoteFeed = null;
                for( let i in feeds ) {
                    if( feeds[ i ] !== null && feeds[ i ] !== "undefined" && feeds[i].rfid == unpublished   ) {
                        remoteFeed = feeds[ i ];
                        break;
                    }
                }
                if( remoteFeed !== null ) {
                    this.OnRemoteLeave(remoteFeed);
                    feeds[remoteFeed.rfindex] = null;
                    remoteFeed.detach();
                }
            }
            if( jsep !== 'undefined' && jsep !== null ) {
                this.sfutest.handleRemoteJsep({ jsep }); 
            }
        } 



            
    }
   
    NewRemoteFeed( e, audio, video  ) {
        const { feeds , opaqueId, myroom, mypvtid  } = this;
        const { display , id } = e; 
        var remoteFeed = null;
        var data = JSON.parse( display );
        const { pseudo } = display;
        var user = {
            ...USERMODEL,
            pseudo,
            id: Number( id )
        };
        //console.log( 'NEW FEED from ' + id )
        this.janus.attach({
            plugin: "janus.plugin.videoroom",
			opaqueId,
            success: ( pluginHandle )=> {
                remoteFeed = pluginHandle;
				remoteFeed.simulcastStarted = false;
                remoteFeed.user = user;
                var subscribe = {
					request: "join",
					room: myroom,
					ptype: "subscriber",
					feed: id,
					private_id: mypvtid
				};
                if(Janus.webRTCAdapter.browserDetails.browser === "safari" &&
						(video === "vp9" || (video === "vp8" && !Janus.safariVp8))) {
					if(video)
						video = video.toUpperCase()
					subscribe["offer_video"] = false;
				}
                //remoteFeed.videoCodec = video;
				remoteFeed.send({ message: subscribe });
            }, //end success
            error: (e) => {  },
            onmessage: ( msg, jsep ) => {
                const { videoroom = null, error = null } =  msg;
                //console.log('MMMM', msg)
                
                if( msg["error"]) {

                } else if ( videoroom === 'attached') {
                    for(var i=1 ; i< MAX_USERS ; i++) {
                        if(!feeds[i]) {
                            feeds[i] = remoteFeed;
                            remoteFeed.rfindex = i;
                           break;
                        }
                    }
                    remoteFeed.rfid = Number(msg["id"]);
					remoteFeed.rfdisplay = msg["display"];
                    remoteFeed.user = {
                        ...USERMODEL,
                        ...JSON.parse( msg['display'] ),
                        id:  Number(msg["id"]),
                    }


                } else if ( videoroom === 'event' ) {

                } else {

                }
                if(jsep) {
                    //console.log('SDP', jsep.sdp ); 
					var stereo = (jsep.sdp.indexOf("stereo=1") !== -1);
					// Answer and attach
					remoteFeed.createAnswer(
						{
							jsep,
							media: { audioSend: false, videoSend:  false },	// We want recvonly audio/video
							/*customizeSdp: function(jsep) {
								if(stereo && jsep.sdp.indexOf("stereo=1") == -1) {
									jsep.sdp = jsep.sdp.replace("useinbandfec=1", "useinbandfec=1;stereo=1");
								}
							},*/
							success: function(jsep) {
                                //console.log('YYYYYYYYY', myroom ); 
								var body = { request: "start", room: myroom };
								remoteFeed.send({ message: body, jsep });
							},
							error: function(error) {
								//console.log( error ); 
							}
						});
				}


            } //end onmessage
            ,  onremotestream: (stream) => {
                //console.log('remote stream' )
                this.onRemoteStream( stream, remoteFeed.user, remoteFeed.rfid ); 
                
            },
            oncleanup: () => {

            }
        }); //end janus attach

    }

    async Publish(useAudio = true) {
        const { sfutest } = this; 
        var _this = this; 
        
        var video = {
            facingMode: 'environment',
            /*height: { ideal: 720, min: 720, max: 1080 },
            width: { ideal: 1280 , min: 720, max: 1280},*/

            iceRestart: true
        }
        var media = { audioRecv: false, videoRecv: false, audioSend: useAudio, videoSend: true, video: true   };
        sfutest.createOffer(
            {
            
                media: media,
                offerToReceiveAudio: true, offerToReceiveVideo: true,
                success: function(jsep) {
                    Janus.debug("Got publisher SDP!");
                    Janus.debug(jsep);
                    var publish = { request: "configure", audio: useAudio, video: true };
        
                    sfutest.send({"message": publish, jsep});
                },
                error: function(error) {
                    Janus.error("WebRTC error:", error);
                    if (useAudio) {
                         _this.Publish(false);
                    } else {
                        
                       // $('#publish').removeAttr('disabled').click(function() { publishOwnFeed(true); });
                    }
                }
            });
    }
    async ToogleMute() {
        const { sfutest } = this; 
        var muted = sfutest.isAudioMuted();
        if(muted)
		    sfutest.unmuteAudio();
	    else
		    sfutest.muteAudio();
	    muted = sfutest.isAudioMuted();
    }

    unpublishOwnFeed() {
        var unpublish = { "request": "unpublish" };
        this.sfutest.send({"message": unpublish});
    }

    Join() {
        const display = { ...this.user_data, id: this.myid };
        var register = {
            request: 'join',
            room: this.myroom,
            ptype: 'publisher',
            display: JSON.stringify( display )
        }
        //console.log( register ); 
        this.sfutest.send({ message: register });
    }
}


