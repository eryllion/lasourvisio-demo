/* eslint-disable */
import React from 'react'; 
import { Cache } from './Cache.js'; 
import {v4 } from 'uuid' 


export const Uuid = async () => {
    
    var { uuid = false } = await Cache.getItem('uuid');
    if(  uuid !== false ) return uuid;
    uuid = v4();
    Cache.setItem('uuid', { uuid });
    return uuid; 
}