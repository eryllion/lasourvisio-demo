/* eslint-disable */
import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLocationArrow} from '@fortawesome/free-solid-svg-icons';

export class Loading extends React.Component {
    static defaultProps = {

    }
    constructor( props ) {
        super( props );
        this.state = {
            loaded: false
        }
    }
    componentDidMount() {
        this.setState({ loaded: true });
    }
    render() {
        return(
            <div id="Loading" className="flex flexOne">

                <div className="flex flexOne box-center ">
                    <div style={{ fontSize: 120 }}>
                        <i className="fas fa-spinner fa-spin"></i>
                    </div>
                </div>

            </div>
        );
    }
}