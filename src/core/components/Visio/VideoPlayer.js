/* eslint-disable */
import { relativeTimeThreshold } from 'moment';
import React from 'react';
import { Store  } from '../../lib/index.js';


export class VideoPlayer extends React.Component {
    static defaultProps = {
        data: { pseudo : '',  stream: false, uuid: ''  },
    }
    constructor( props ) {
        super( props );
        this.state = {};
        this.stream = this.props.data.stream;
        this.player = React.createRef();
        this.video = React.createRef();
        this.loaded = false; 

    }
    componentDidMount() {
        
        this.SetStream( this.stream );
    }
    render() {
        var { uuid } = this.props.data;
        return (
            <div className="visios" id={"visio-" + uuid } >
                <div className="video-player" ref={this.player} id={ "video-player-" + uuid }>
                    <video autoPlay controls ref={this.video} 
                        id={"player-" + uuid}
                        disablePictureInPicture
                        onLoadedMetadata={() => this.onMediaLoaded()}
                    
                    >
                    </video>
                </div>
            </div>
        );
    }
    onMediaLoaded() {
       this.player.current.style.display = "block";
       
       this.loaded = true; 
    }

    async SetStream( stream ) {
        this.stream = stream;
        if( this.stream !== false && this.stream !== null ) {
            Janus.attachMediaStream( this.video.current , this.stream);
            
        }
    }
}