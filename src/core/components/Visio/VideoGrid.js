/* eslint-disable */
import React, { forwardRef } from 'react';
import ReactDOM from 'react-dom';
import '../../styles/VideoGrid.css';
import { VideoRoomClass } from '../../lib/VideoRoom/VideoRoom.js';
import { VideoPlayer } from '..';


export class VideoGrid extends React.Component {
    static defaultProps = {
       
    }
    constructor( props ) {
        super( props );
        this.state = {
            loaded: false
        }
        this.list = [];
        this.visios = {};
        this.Grid = React.createRef();
    }
    componentDidMount() {
        this.setState({ loaded: false , visios: [ ] });
       
    }
    render() {
        return(
            <div id="visioroom-container" ref={this.Grid}>
                <div id="close-button" onClick={() => this.LeaveRoom()}>
                    <i className="fas fa-power-off"></i>
                </div>
                {this.renderVisios()}
            </div>
        );
    }

    renderVisios() {
        const { loaded , visios } = this.state; 
        var tmp = [];
        if( this.state.loaded === false ) return false;  
        for( let  i in visios ) {
            const d = visios[ i ];
            tmp.push( <VideoPlayer  data={d}  key={"v-" + d.uuid} />)
        }
        this.Grid.current.classList.remove('grid1', 'grid2', 'grid3' , 'grid4');
        this.Grid.current.classList.add( 'grid' + Object.keys( this.state.visios ).length ) ;


        return tmp;
    }
    

    async LoadVisio(me, roomId ) {
        this.VideoRoom = new VideoRoomClass( roomId ); 
        this.VideoRoom.OnListChanged = ( users  ) => {
           
            this.setState({ 
                visios: users, loaded: true 
            })
           
        } 
        this.VideoRoom.OnDelParticipant = (uuid, users ) => {
            this.setState({ 
                visios: users, loaded: true
            })
           
        }
        this.VideoRoom.OnMessage = () => {};
        this.VideoRoom.user_data = me;  
       this.VideoRoom.Init();

    }

    

    async DrawVisios( list = [] ) {
        /*list = [
            { uuid: '12' },
            { uuid: '34' },
            { uuid: '56' },
            { uuid: '78' }
        ];*/
        let tmp = [];
        
        

        for( let i in list ) {
            var element = null; 
            const c = list[ i ];
            const { uuid } = c;
            if( typeof this.visios[ uuid ] !== 'undefined' ) {
                this.visios[ uuid ].ref.current.SetStream( c.stream );
                element = this.visios[ uuid ].element; 
                
            } else {
                var ref = React.createRef();
                element = <VideoPlayer data={c} ref={ref}/>
                this.visios[ uuid ] = {
                    data: c,
                    ref,
                    element
                }            
            }
            tmp.push( element   );
        }

        this.Grid.current.classList.remove('grid1', 'grid2', 'grid3' , 'grid4');
        this.Grid.current.classList.add( 'grid' + Object.keys( list ).length ) ;

       ReactDOM.render( tmp, this.Grid.current );
    }
   
   

    async Init(config ) {
        const { user_data , roomId } = config; 
        this.user_data = user_data;
        this.roomId = roomId;
        this.LoadVisio(user_data , roomId);
       
    }

    async LeaveRoom() {
        this.VideoRoom.WebRTC.Stop( () => {
            document.location.reload();
        })
    }

}