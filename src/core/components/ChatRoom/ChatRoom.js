/* eslint-disable */
import React from 'react';
import ReactDOM from 'react-dom'; 
import '../../styles/ChatRoom.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLocationArrow} from '@fortawesome/free-solid-svg-icons';
import moment from 'moment'; 
import { Store, Events , WServer } from '../../lib/index.js';

export class ChatRoom extends React.Component {
    static defaultProps = {
        visio: { pseudo : '' , roomId: ''  }
    }
    constructor( props ) {
        super( props );
        this.state = {
            loaded: false,
            showRules: false,
            acceptRules: false,
            showInput: false, 
        }
        this.chat = React.createRef();
        this.chatRules = React.createRef();
        this.input = React.createRef();
        this.chatContent = React.createRef(); 
        this.messages = []; 
        this.roomId = this.props.visio.roomId; 
        
    }
    componentDidMount() {
        this.setState({ loaded: true });
        Events.on('ROOM-ON-MESSAGE' , 'ChatRoom' , (data) => {
            this.DrawMessage( data );
        })
    }
    render() {
        return (
            <div id="visio-chat-container" className="flex flexOne column" >
                {this.renderChatParent()}
                {this.renderChatHandler()}
            </div>
        );
    }
    renderChatParent() {
        return (
            <div id="chat-parent" ref={this.chatContent} >
                <div id="visio-chat-reduce-handler" >
                    <div className="isopened" >
                        <i className="fas fa-chevron-down" ></i>
                        Cacher le chat
                    </div>
                    <div className="isclosed" >
                        <i className="fas fa-chevron-up" ></i>
                        Voir le chat
                    </div>
                </div>
                <div id="chat" ref={this.chat}>
                    <div id="chat-content" >

                    </div>
                </div>
             </div>);
    }
    async JoinHandle () {
        
        this.setState({ showRules: true }); 
    }    

    renderChatHandler () {
        return (
            <div id="chat-handle">
                <button className="btn btn-white" id="chat-join-btn" onClick={() => this.JoinHandle( ) }
                    style={{ display: ( this.state.showRules ? 'none' : 'block')}}
                >
                  Rejoindre le Chat
                </button>
                <div id="chat-rules" style={{ display: ( !this.state.showRules  ? 'none' : 'block') }}
                    ref={this.chatRules}
                
                >
                    {this.renderChatRules()}
                    
                    
                </div>
                {this.renderInputContainer()}
            </div>
            
        )
    }
    renderChatRules() {
        if( this.state.showRules === false) return false;
        const AcceptHandle = () => {
            this.setState({ acceptRules: true }); 
            this.chatRules.current.remove();
         }
        return (
            <div>
                <div id="chat-rules-head">
                        <div id="chat-rules-title">Règles</div>
                        <div id="chat-rules-close"> <i className="fas fa-times"></i> </div>
                    </div>
                    <div id="chat-rules-textes">
                        - DITES BONSOIR <br />
                        - Respectez tout le monde <br />
                        - PAS DE SPAM DE MAJUSCULE<br />
                        - Pas de lien<br />
                        - Pas de sujet tabou ou lourd (religion, racisme, politique...)<br />
                        - Pas de langage SMS
                    </div>
                    <input style={{display: 'none'}} type="text" name="username" 
                    placeholder="Saisissez un pseudo" id="chat-rule-username"
                    />
                    <button className="btn btn-white" id="chat-accept-btn"
                        onClick={() => AcceptHandle() }
                    > J'accepte et je rejoins  </button>
            </div>
        )
    }
    renderInputContainer() {
        if( !this.state.acceptRules ) return false;
        
        return (
            <div id="chat-input-container" className="flex flexOne row" >
            <input type="text" id="chat-input" autoComplete="off" ref={this.input} />
            <button className="btn btn-white" id="chat-btn" onClick={() => this.SendMessage()}> 
                <FontAwesomeIcon icon={faLocationArrow}  />
            </button>
        </div>
        )
    }

    async SendMessage() {
        var message = this.input.current.value; 
        var hour = await moment().format('HH:mm'); 
        var message = {
            message,
            hour,
            pseudo: this.props.visio.pseudo ,
            uuid: Store.uuid
        }
        

        WServer.Emit( 'room-webrtc', { task: 'new-message', message, roomId: this.roomId } );
        this.input.current.value = ""; 
    }

    DrawMessage( element ) {
        let { pseudo = false, message , hour, uuid   } = element;
        if( pseudo === false ) {
            pseudo = this.props.visio.pseudo ;
        }
        var height = jQuery('#chat-parent').height();
        var line = (
            <div className="chat-line" key={moment().format('x')} >
                <span className="chat-line-hour" > 
                    {hour}
                </span>
                <span className={ "chat-line-pseudo " + ( uuid == Store.uuid ? 'chat-current' : '' )  } >{pseudo}: </span>
                <span className="chat-line-text" >{message}</span>
            </div>
        )
        this.messages.push( line ); 
        
        ReactDOM.render( (<div >{this.messages}</div> )  , document.getElementById('chat-content')
        , () => { this.chat.current.style.maxHeight = height + 'px'; } ); 
    }
    
}