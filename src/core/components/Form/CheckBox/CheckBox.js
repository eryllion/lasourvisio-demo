import React from 'react';

export class CheckBox extends React.Component {
    static defaultProps = {
        className: '',
        label: '', value: '', 
        checked: false, 
    } 
    constructor ( props ) {
        super( props );
        this.state = {
            value : this.props.value,
            checked: this.props.checked 
        }
        this._checkbox = React.createRef();
    }
    render() {
        const { className, label, value, checked  } = this.props; 
        return (
            <div className={ "CheckboxContainer " + className } >
               <label className="layout" >
                   <span className="checkbox-input-container" >
                       <input type="checkbox " /> 
                   </span>
               </label>
            </div>
        )
    
    }
}