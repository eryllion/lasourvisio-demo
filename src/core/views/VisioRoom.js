/* eslint-disable */
import React from 'react';
import '../styles/VisioRoom.css';
import '../styles/VideoPlayer.css'; 

import  logo from '../../logo.jpg';
import { array_chunk, Events, Store, WServer  } from '../lib/index.js';
import jQuery from 'jquery'; 
import { ChatRoom, VideoPlayer, VideoGrid, g   } from '../components/index.js';
import { VideoRoomClass, USERMODEL  } from '../lib/VideoRoom/VideoRoom.js'; 
import { Loading  } from '../components/Loading/Loading.js';




export class VisioRoom extends React.Component {
    static defaultProps = {
        roomId: 2000,
        pseudo: ''
    }
    constructor( props ) {
        super( props );
        this.state = {
            loaded: false,
            exceeded: false ,
            visioWidth: 0,
            users : [],
        }
        this.videoGrid = React.createRef();
        this.ChatIcon = React.createRef();
    }
    componentDidMount() {
        const { pseudo, roomId } = this.props;
        var visioWidth = jQuery("#visioroom-container").width();
       
        var user_data = { ...USERMODEL , 
            pseudo,
            uuid: Store.uuid
        }
        this.user_data  = user_data;
     
        WServer.socket.emit('join-room', { ...user_data, username : pseudo, type: "publisher", roomId  }, (e) => {
            if( e.etat === 200 ) {
                    this.videoGrid.current.Init( { user_data, roomId: this.props.roomId  }); 
                    this.setState({ loaded: true, visioWidth });
        
                
            } else if ( e.etat === 201) {
                this.setState({ loaded: true,  exceeded : true }); 
            }
        });
 
    }
    render() {
        //if ( this.state.loaded === false ) return this.renderLoading();  
        if( this.state.exceeded ) return this.Ex(); 

        return(
            <div className="flex flexOne row" id="VisioRoom-view"> 
                {this.renderLeft()}
                {this.renderRight()}
                {this.renderLoading()}
            </div>
        );
    }
    Ex() {
        return (<div className="flex flexOne full-page box-center" >
            <h5>Nombre maximal de participants atteint.</h5>
        </div>)
    }
    renderLoading() {
        if( this.state.loaded ) return false; 
        return (<div className="flex flexOne full-page" id="visioroom-loader">
            <Loading />
        </div>)
    }
    renderLeft() {
        return (<div id="visioroom-left" className="column" >
            
            <div id="visioroom-top" className="flex" >
                <div className="row  flex" id="visioroom-top-content" >
                    <img id="visioroom-logo" src={logo}/>
                    <span className="flex " >LASOUR VISIO </span>
                </div>
                <div className="flex flexOne jc-end" >
                    <div id="visioroom-chat-icon" onClick={() => this.ToogleChatHandle()} ref={this.ChatIcon} >
                        <i className="far fa-comment-alt" ></i>
                    </div>
                </div>
                
            </div>
            
            <VideoGrid  ref={this.videoGrid}  />            
            
            <div id="visioroom-bottom-bar">
                2021 | &copy; Created By Mary LASOUR
            </div>

        </div>)
    }
    renderRight() {
        return (<div id="visioroom-right" className="column" >
            <ChatRoom visio={{ pseudo: this.props.pseudo , roomId : this.props.roomId  }} />
        </div>)
    }

    ToogleChatHandle() {
        var right = jQuery('#visioroom-right');
        right.toggleClass( 'opened' );
       
    }
    
 



}