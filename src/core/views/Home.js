/* eslint-disable */
import React from 'react';
import '../styles/Home.css';

const { PUBLIC_URL } = process.env;
import  logo from '../../logo.jpg';

import { Checkbox, FormControlLabel  , Typography, Button, Link } from '@material-ui/core';
import { InputForm } from '../components/index.js'; 
import { SeparatorText } from '../components/SeparatorText/SeparatorText.js';

import TextField from '@material-ui/core/TextField';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'; 
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import jQuery  from 'jquery';
import { IsNumber } from '../lib';


export class Home extends React.Component {
    static defaultProps = {
        callBack: () => {}
    }
    constructor( props ) {
        super( props );
        this.state = {
            loaded: false,
            showRoomInput: false
        }
    }
    componentDidMount() {
        this.setState({ loaded: true });
    }
    render() {
       
        return(
            <div className="flex" id="Home-View">
                <div id="home-left" className="flex flexOne box-center column">
                    <h1 id="home-title">
                        LASOUR VISIO
                    </h1>
                    <img src={logo} id="home-logo"/>
                </div>
                <div id="home-right" className="flex flexOne p-relative box-center">
                    {this.renderForm()}
                    <span className="p-absolute" id="home-legacy">
                        2021 | &copy; created by Mary LASOUR
                    </span>
                </div>
            </div>
        );
    }
    
    renderForm() {
        this.refs = {
            roomkey: false,
            pseudo: false,
        }
        const JoinRoomBtnClick = function() {
            jQuery('#home-joinroom-btn').slideUp();
            jQuery('#home-roomkey-div').slideDown(); 
            jQuery('#home-joinroomnow-btn').slideDown();
        }

        return(
            <div id="home-form">
                <div className="line-margin" key={"input-form-0"  } >
                    <InputForm  datas={{  label : 'Pseudo' , name: "pseudo" , value: '', require: true }} ref={ e => this.refs.pseudo = e } />
                 </div>
                 <SeparatorText  />

                 <button className="cButton button-disabled" onClick={ () => this.CreateRoom() }>
                     Créer une salle (désactivé)
                 </button>
                 <SeparatorText text="OU" />

                 <button className="cButton" onClick={  JoinRoomBtnClick } id="home-joinroom-btn">
                     Joindre une salle
                 </button>

                 <div className="line-margin" key="input-form-1" id="home-roomkey-div" >
                    <InputForm  datas={{  label : 'ID De la salle' , name: "roomKey" , value: '', require: true }} ref={ e => this.refs.roomKey = e } />
                 </div>

                 <button className="cButton" onClick={ () => this.JoinRoom() } id="home-joinroomnow-btn">
                     Rejoindre la salle maintenant
                 </button>
            </div>
        );
    }

    

    async CreateRoom() {
        alert( "Cette action n'est pas autorisé pour le moment ! " );
        jQuery('#home-roomkey-div').slideUp(); 
        jQuery('#home-joinroom-btn').slideDown(); 
        jQuery('#home-joinroomnow-btn').slideUp(); 
    }
    async JoinRoom() {
        var roomId = await this.refs.roomKey.Val() ;
        const pseudo = await this.refs.pseudo.Val() ;
        roomId = Number( roomId );
        var rooms = [ 2000, 2001, 2002, 2003 ]; //, 3000, 4000 ];
        if( pseudo === '' ) {
            alert("Pseudo Incorrect"); 
            return false;
        }

        if( !IsNumber( roomId  ) || rooms.indexOf( roomId ) === -1) {
            alert("La room est incorrect"); 
            return false;
        }
        
       this.props.callBack({ roomId, pseudo });
       
    }
}