export const author = "Mary LASOUR"; 


export const  CONFIG = {
    APP_KEY: 'lasour-visio-demo',

    socket_api_connect: { host:  'https://lasour.sytes.net', port: 801 }, 
    socket_settings: { 
        transports: [ 'websocket'], 'force new connection': true,
        reconnection: false, //true, 
        reconnectionDelay: 100, //10000, 
        reconnectionDelayMax: 60000,
        reconnectionAttempts: 'Infinity', 
        timeout: 2000, ///10000, 
        rejectUnauthorized:   false,
        enabledTransports: [ "ws", "wss"]
    }
}; 