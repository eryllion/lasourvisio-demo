/* elslint-disable */

import React from 'react';
import { Store, Uuid, WServer, Events } from './lib/index.js';
import { Home, VisioRoom } from './views/index.js'; 
import { Loading  } from './components/Loading/Loading.js';


/**** CORE  */
export class Core extends React.Component {
    static defaultProps = {}

    constructor( props ) {
        super( props );
        this.state = {
            loaded: false,
            current: 'home'
        }
        this.user_data = { pseudo: '' , roomId : '' }; 
    }
    componentDidMount() {
        this.Init(); 
    }
    render() {
        if( !this.state.loaded ) return this.renderLoading();
        return(
            <div className="flex flexOne full-page">
               {this.renderHome()}
               {this.renderVisio()}    
            </div>
        );
    }
    renderHome() {
        if( this.state.current !== 'home') return false;
        return <Home callBack={(e) => this.HomeCallBack(e) } />;
    }

    renderVisio() {
        if( this.state.current !== 'visio') return false;
        const { pseudo , roomId } = this.user_data; 
        return <VisioRoom pseudo={pseudo} roomId={roomId}  />;
    }

    renderLoading() {
        return (<div className="flex flexOne full-page">
            <Loading />
        </div>)
    }

    async HomeCallBack({ pseudo , roomId }) {
        this.user_data = { pseudo , roomId }; 
        
        this.setState({ current: 'visio' }) ; 
    }

    /**
     * Initialize The App 
     * Connect to API
     *
     */
    async Init() {
        if( Store.uuid === false   ) {
            Store.uuid = await Uuid(); //create a new UUID
        }
        /** wserver */
        Events.on('SOCKET_CONNECTED', 'Core', () => {
            this.setState({ loaded : true });
        });
        WServer.Connect({ query: { }, uuid: Store.uuid  });
   }
}



export default Core; 